package com.devcamp.j04_javabasic.s50;

import java.util.ArrayList;

import com.devcamp.j04_javabasic.s10.CCat;
import com.devcamp.j04_javabasic.s10.CDog;
import com.devcamp.j04_javabasic.s10.CPerson;
import com.devcamp.j04_javabasic.s10.CPet;

public class CMain {

 public static void main(String[] args) {
  CPet dog = new CDog(1, "Lulu");

  CPet cat = new CCat(2,"Pussy");
  ArrayList<CPet> petsList = new ArrayList<>();
  petsList.add(dog);
  petsList.add(cat);

  CPerson person1 = new CPerson();
  CPerson person2 = new CPerson( 1,20,"Chung Chi","Son",petsList);
  System.out.println("Fullname = " + person1.getFirstName());
  System.out.println(person2);
  System.out.println("Age = " + person2.getAge());
 }
}
