package com.devcamp.j04_javabasic.s10;

public class CFish extends CPet implements ISwimable {

    @Override
    public void swim() {
        // TODO Auto-generated method stub
        System.out.println("Fish swimming...");    
    }
    
    public void print() {
        System.out.println("Fish print!");
    }

    public void play() {
        System.out.println("Fish play!");
    }
}
