package com.devcamp.j04_javabasic.s10;

public class CBird extends CPet implements IFlyable{

    @Override
    public void fly() {
        // TODO Auto-generated method stub
        System.out.println("Bird flying...");
    }
    
    public void print() {
        System.out.println("Bird print!");
    }

    public void play() {
        System.out.println("Bird play!");
    }

    @Override
    public void animalSound() {
        // TODO Auto-generated method stub
        System.out.println("Bird sound!");
    }   
}
